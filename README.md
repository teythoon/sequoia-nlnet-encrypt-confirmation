An encryption point-solution for a Perl web service
===================================================

This produces a shared object that exposes a single function to
encrypt a given message with a given certificate.  This function can
be easily invoked using a FFI solution, e.g. Perl's Platypus.  See
`example.pl` for inspiration.

Building
--------

This library is backed by [Sequoia PGP](https://sequoia-pgp.org) and
the cryptographic library Nettle.  To build it, you will need to
install some build dependencies.  On a Debian system, this can be done
using

```sh
$ sudo apt install rustc cargo clang libclang-dev pkg-config nettle-dev
```

Then, building the library is a matter of

```sh
$ cargo build --release
[... grab a coffee...]
$ file target/release/libsequoia_nlnet_encrypt_confirmation.so
target/release/libsequoia_nlnet_encrypt_confirmation.so: ELF 64-bit LSB shared object, x86-64, version 1 (SYSV), dynamically linked, BuildID[sha1]=91396c1ed88f036e1b8ddd8878367bdcbb900185, not stripped
```

Example
-------

```
$ LD_LIBRARY_PATH=target/release perl example.pl
-----BEGIN PGP MESSAGE-----

wV4DR2b2udXyHrYSAQdAenGZCDKSJF+7iH8lbXRS/KQyn2uYSWBHK8sNjXS/u34w
KYDwyFXxIeuSErDsqV9Z+cMnzGNQNAqaqcUVLQBytdD1PivDixr+lMf5k/0z2N7w
0kABlXQj1sFwomyab4JMWj8T0LYgU+T+FyqbtMg48FuaCahLJQkfXMb1Hpk14JBF
ML9cCGNIQJJq0iNpScd+xH2Y
=89sb
-----END PGP MESSAGE-----
$ LD_LIBRARY_PATH=target/release perl example.pl | sqop decrypt alice.sec.pgp
secret message
```
