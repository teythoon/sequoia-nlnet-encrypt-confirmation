use std::{
    io,
    panic,
    ptr,
    slice,
};

use sequoia_sop::{
    SQOP,
    sop::{
        SOP,
        Result,
    },
};

#[no_mangle] pub unsafe extern "C"
fn sequoia_nlnet_encrypt(cert: *const u8,
                         cert_len: usize,
                         plaintext: *const u8,
                         plaintext_len: usize,
                         ciphertext: *mut *mut u8,
                         ciphertext_len: *mut usize,
                         error: *mut *mut u8,
                         error_len: *mut usize)
                         -> bool
{
    let cert = slice::from_raw_parts(cert, cert_len);
    let plaintext = slice::from_raw_parts(plaintext, plaintext_len);
    match panic::catch_unwind(|| encrypt(cert, plaintext)) {
        Ok(Ok(v)) => {
            to_buffer(v, ciphertext, ciphertext_len);
            *error = ptr::null_mut();
            *error_len = 0;
            false
        },
        Ok(Err(e)) => {
            to_buffer(e.to_string(), error, error_len);
            *ciphertext = ptr::null_mut();
            *ciphertext_len = 0;
            true
        },
        Err(_) => {
            to_buffer("panic", error, error_len);
            *ciphertext = ptr::null_mut();
            *ciphertext_len = 0;
            true
        },
    }
}

fn encrypt(cert: &[u8], plaintext: &[u8]) -> Result<Vec<u8>>
{
    let mut cert = io::Cursor::new(cert);
    let mut plaintext = io::Cursor::new(plaintext);
    let ciphertext = SQOP::default().encrypt()?
        .with_cert(&mut cert)?
        .plaintext(&mut plaintext)?
        .to_vec()?;
    Ok(ciphertext)
}


unsafe fn to_buffer<D: AsRef<[u8]>>(data: D, buf: *mut *mut u8, len: *mut usize)
{
    let data = data.as_ref();
    *len = data.len();
    *buf = libc::malloc(*len) as *mut _;
    slice::from_raw_parts_mut(*buf, *len)
        .copy_from_slice(data);
}
