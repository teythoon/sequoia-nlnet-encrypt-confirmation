#!/usr/bin/perl

use strict;
use FFI::Platypus 1.00;
use FFI::CheckLib qw( find_lib_or_die );
use FFI::Platypus::Buffer qw( scalar_to_buffer buffer_to_scalar );
use FFI::Platypus::Memory qw( free );

my $ffi = FFI::Platypus->new( api => 1 );
$ffi->lib(find_lib_or_die lib => 'sequoia_nlnet_encrypt_confirmation');

$ffi->attach(
  [ sequoia_nlnet_encrypt => 'encrypt' ] => [
    'opaque',                           # cert
    'unsigned int',                     # cert len
    'opaque',                           # plaintext
    'unsigned int',                     # plaintext len
    'opaque *',                         # ciphertext
    'unsigned int *',                   # ciphertext len
    'opaque *',                         # error
    'unsigned int *',                   # error len
  ] => 'int',
  sub {
    my $sub = shift;
    my ($cert, $cert_len) = scalar_to_buffer $_[0];
    my ($plaintext, $plaintext_len) = scalar_to_buffer $_[1];
    my $ciphertext = undef;
    my $ciphertext_len = 0;
    my $error = undef;
    my $error_len = 0;
    my $r = $sub->($cert, $cert_len, $plaintext, $plaintext_len,
		   \$ciphertext, \$ciphertext_len,
		   \$error, \$error_len);
    if ($r eq 1) {
	my $e = buffer_to_scalar($error, $error_len);
	free $error;
	die "Encryption failed: $e";
    } else {
	my $c = buffer_to_scalar($ciphertext, $ciphertext_len);
	free $ciphertext;
	$c;
    }
  },
);

my $cert = <<'END_CERT';
-----BEGIN PGP PUBLIC KEY BLOCK-----
Comment: Alice's OpenPGP certificate

mDMEXEcE6RYJKwYBBAHaRw8BAQdArjWwk3FAqyiFbFBKT4TzXcVBqPTB3gmzlC/U
b7O1u120JkFsaWNlIExvdmVsYWNlIDxhbGljZUBvcGVucGdwLmV4YW1wbGU+iJAE
ExYIADgCGwMFCwkIBwIGFQoJCAsCBBYCAwECHgECF4AWIQTrhbtfozp14V6UTmPy
MVUMT0fjjgUCXaWfOgAKCRDyMVUMT0fjjukrAPoDnHBSogOmsHOsd9qGsiZpgRnO
dypvbm+QtXZqth9rvwD9HcDC0tC+PHAsO7OTh1S1TC9RiJsvawAfCPaQZoed8gK4
OARcRwTpEgorBgEEAZdVAQUBAQdAQv8GIa2rSTzgqbXCpDDYMiKRVitCsy203x3s
E9+eviIDAQgHiHgEGBYIACAWIQTrhbtfozp14V6UTmPyMVUMT0fjjgUCXEcE6QIb
DAAKCRDyMVUMT0fjjlnQAQDFHUs6TIcxrNTtEZFjUFm1M0PJ1Dng/cDW4xN80fsn
0QEA22Kr7VkCjeAEC08VSTeV+QFsmz55/lntWkwYWhmvOgE=
=iIGO
-----END PGP PUBLIC KEY BLOCK-----
END_CERT

my $ciphertext = encrypt($cert, "secret message\n");
print "$ciphertext";
